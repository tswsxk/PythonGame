# -*- coding:utf-8 -*-

def story():
  print "从前有座山,山里有座庙,庙里有个老和尚,在给小和尚讲故事.讲的什么故事呢?"
  print "从前有座山,山里有座庙,庙里有个老和尚,在给小和尚讲故事.讲的什么故事呢?"
  print "从前有座山,山里有座庙,庙里有个老和尚,在给小和尚讲故事.讲的什么故事呢?"
  print "从前有座山,山里有座庙,庙里有个老和尚,在给小和尚讲故事.讲的什么故事呢?"
  print "从前有座山,山里有座庙,庙里有个老和尚,在给小和尚讲故事.讲的什么故事呢?"

def while_story():
  while True:
    print "从前有座山,山里有座庙,庙里有个老和尚,在给小和尚讲故事.讲的什么故事呢?"
def var_story():
  mountain_story = "从前有座山,山里有座庙,庙里有个老和尚,在给小和尚讲故事.讲的什么故事呢?"
  print mountain_story
def new_story(mountain_name):
  mountain_story = "从前有座" + mountain_name + ",山里有座庙,庙里有个老和尚,在给小和尚讲故事.讲的什么故事呢?"
  print mountain_story

def branch_story(week_day):
  if 1 <= week_day <= 3:
    mountain_name = "大蜀山"
  elif 4 <= week_day <= 5:
    mountain_name = "黄山"
  else:
    mountain_name = "五指山"
  mountain_story = "从前有座" + mountain_name + ",山里有座庙,庙里有个老和尚,在给小和尚讲故事.讲的什么故事呢?"
  return mountain_story

if __name__ == "__main__":
  week_day_name = ["一", "二", "三", "四", "五", "六", "天"]
  for i in range(7):
    print "今天是星期" + week_day_name[i] + ": " + branch_story(i + 1)
