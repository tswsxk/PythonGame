# -*- coding:utf-8 -*-

def generate_number():
  # 第一部分，生成数字
  # python 随机数
  import random
  return random.randint(0, 1000)

def getinput():
  # 第二部分：接收玩家输入
  # python 屏幕输入
  numStr = raw_input("请玩家输入数字: ")
  # 强制类型转换
  input_number = int(numStr)
  return input_number

def tips_to_player(msg):
  # 第三部分，给玩家提示
  print msg

def game():
  # 猜数字游戏

  number = generate_number()
  minnum = 0
  maxnum = 1000
  tips_to_player("数字在 " + str(minnum) + " 到 " + str(maxnum) + " 之间," +
                 "还剩" + str(12) + "次机会")
  for guess_time in range(12):
    # 玩家一共有12次猜测机会，当循环结束时，游戏失败
    guess_num = getinput()
    if guess_num == number:
      # 猜对数字
      tips_to_player("游戏胜利(⊙v⊙)，你用了" + str(guess_time + 1) + "次猜对了答案，答案就是" + str(guess_num))
      return
    elif guess_num < minnum or guess_num > maxnum:
      tips_to_player("请认真猜(｡ì _ í｡)," + "还剩" + str(11 - guess_time) + "次机会")
    else:
      if guess_num < number:
        minnum = guess_num + 1
      else:
        maxnum = guess_num - 1
      tips_to_player("数字在 " + str(minnum) + " 到 " + str(maxnum) + " 之间," +
                     "还剩" + str(11 - guess_time) + "次机会")
  tips_to_player("游戏失败╮(╯_╰)╭，" + "答案是" +str(number))

if __name__ == "__main__":
  game()


