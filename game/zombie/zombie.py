# -*- coding:utf-8 -*-
# 指明编码，没有上面这一句，程序里是不能出现中文的，这句必须放在开头

import pygame, random, sys, time
from pygame.locals import *

# 默认参数设定，游戏窗口的大小之类的
# 窗口的宽度、高度
WINDOWWIDTH = 1024
WINDOWHEIGHT = 600

FPS = 60

# 游戏参数
# 最多可以有几只僵尸被放过
MAXGOTTENPASS = 10
# 僵尸尺寸
ZOMBIESIZE = 70

# 两种僵尸出现的速度
ADDNEWZOMBIERATE = 30
ADDNEWKINDZOMBIE = ADDNEWZOMBIERATE

NORMALZOMBIESPEED = 2
NEWKINDZOMBIESPEED = NORMALZOMBIESPEED / 2

# 玩家移动速度
PLAYERMOVERATE = 15

# 子弹飞行速度
BULLETSPEED = 10
# 攻击 CD 时间
ADDNEWBULLETRATE = 15

TEXTCOLOR = (255, 255, 255)
RED = (255, 0, 0)

# 函数部分
def terminate():
    # 程序终止
    pygame.quit()
    sys.exit()

def waitForPlayerToPressKey():
    # 用户按键处理
    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                terminate()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE: # 按下退出键ESC
                    terminate()
                if event.key == K_RETURN:
                    return

def playerHasHitZombie(playerRect, zombies):
    # 被僵尸吃掉了
    for z in zombies:
        if playerRect.colliderect(z['rect']):
            return True
    return False

def bulletHasHitZombie(bullets, zombies):
    # 判断子弹有没有击中僵尸
    for b in bullets:
        if b['rect'].colliderect(z['rect']):
            bullets.remove(b)
            return True
    return False

def bulletHasHitCrawler(bullets, newKindZombies):
    for b in bullets:
        if b['rect'].colliderect(c['rect']):
            bullets.remove(b)
            return True
    return False

def drawText(text, font, surface, x, y):
    # 写字
    textobj = font.render(text, 1, TEXTCOLOR)
    textrect = textobj.get_rect()
    textrect.topleft = (x, y)
    surface.blit(textobj, textrect)

# 设置游戏参数
# pygame 初始化
pygame.init()
mainClock = pygame.time.Clock()

# 设定窗口大小、窗口标题、鼠标可见
windowSurface = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))#, pygame.FULLSCREEN)
pygame.display.set_caption('Zombie Defence')
pygame.mouse.set_visible(False)

# 设定显示字体
font = pygame.font.SysFont(None, 48)

# 设置音乐
gameOverSound = pygame.mixer.Sound('Music/gameover.wav')
pygame.mixer.music.load('Music/grasswalk.mp3')

# 设置图片
playerImage = pygame.image.load('Figure/SnowPea.gif')
playerRect = playerImage.get_rect()

bulletImage = pygame.image.load('Figure/SnowPeashooterBullet.gif')
bulletRect = bulletImage.get_rect()

zombieImage = pygame.image.load('Figure/tree.png')
newKindZombieImage = pygame.image.load('Figure/ConeheadZombieAttack.gif')

backgroundImage = pygame.image.load('Figure/background.png')
rescaledBackground = pygame.transform.scale(backgroundImage, (WINDOWWIDTH, WINDOWHEIGHT))

# 开始界面的设置及显示
windowSurface.blit(rescaledBackground, (0, 0))
windowSurface.blit(playerImage, (WINDOWWIDTH / 2, WINDOWHEIGHT - 70))
drawText('Zombie Defence By handsomestone', font, windowSurface, (WINDOWWIDTH / 4), (WINDOWHEIGHT / 4))
drawText('Press Enter to start', font, windowSurface, (WINDOWWIDTH / 3) - 10, (WINDOWHEIGHT / 3) + 50)
pygame.display.update()
waitForPlayerToPressKey()
while True:
    # 游戏开始！

    # 两种僵尸和一种子弹
    zombies = []
    newKindZombies = []
    bullets = []

    # 没有阻止的僵尸
    zombiesGottenPast = 0
    # 当前得分
    score = 0

    # 设定玩家（植物）的初始位置
    playerRect.topleft = (50, WINDOWHEIGHT /2)
    # 底下几个都是标记变量（信号），后面会用到
    moveLeft = moveRight = False
    moveUp = moveDown = False
    shoot = False

    # 计数器变量，后面会用到
    zombieAddCounter = 0
    newKindZombieAddCounter = 0
    bulletAddCounter = 40

    # 播放音乐
    pygame.mixer.music.play(-1, 0.0)

    while True:
        # 游戏运行部分
        for event in pygame.event.get():
            if event.type == QUIT:
                # 退出游戏
                terminate()

            # 检测到有按键按下
            if event.type == KEYDOWN:
                # 上下控制
                if event.key == K_UP or event.key == ord('w'):
                    moveDown = False
                    moveUp = True
                if event.key == K_DOWN or event.key == ord('s'):
                    moveUp = False
                    moveDown = True
                # 发射子弹
                if event.key == K_SPACE:
                    shoot = True

            # 检测到有按键被释放
            if event.type == KEYUP:
                if event.key == K_ESCAPE:
                    # 退出游戏
                    terminate()

                # 上下控制
                if event.key == K_UP or event.key == ord('w'):
                    moveUp = False
                if event.key == K_DOWN or event.key == ord('s'):
                    moveDown = False

                # 停止发射子弹
                if event.key == K_SPACE:
                    shoot = False

        # 在计数器到一定值以后，放置新的僵尸
        zombieAddCounter += 1
        if zombieAddCounter == ADDNEWKINDZOMBIE:
            zombieAddCounter = 0
            zombieSize = ZOMBIESIZE       
            newZombie = {'rect': pygame.Rect(WINDOWWIDTH, random.randint(10, WINDOWHEIGHT-zombieSize-10), zombieSize, zombieSize),
                        'surface':pygame.transform.scale(zombieImage, (zombieSize, zombieSize)),
                        }

            zombies.append(newZombie)

        # 在计数器到一定值以后，放置新的僵尸
        newKindZombieAddCounter += 1
        if newKindZombieAddCounter == ADDNEWZOMBIERATE:
            newKindZombieAddCounter = 0
            newKindZombiesize = ZOMBIESIZE
            newCrawler = {'rect': pygame.Rect(WINDOWWIDTH, random.randint(10,WINDOWHEIGHT-newKindZombiesize-10), newKindZombiesize, newKindZombiesize),
                        'surface':pygame.transform.scale(newKindZombieImage, (newKindZombiesize, newKindZombiesize)),
                        }
            newKindZombies.append(newCrawler)

        # 发射子弹
        # bulletAddCounter 是一个 CD 计数器
        bulletAddCounter += 1
        if bulletAddCounter >= ADDNEWBULLETRATE and shoot is True:
            bulletAddCounter = 0
            newBullet = {'rect':pygame.Rect(playerRect.centerx+10, playerRect.centery-25, bulletRect.width, bulletRect.height),
						 'surface':pygame.transform.scale(bulletImage, (bulletRect.width, bulletRect.height)),
						}
            bullets.append(newBullet)

        # 上下移动玩家
        if moveUp and playerRect.top > 30:
            playerRect.move_ip(0,-1 * PLAYERMOVERATE)
        if moveDown and playerRect.bottom < WINDOWHEIGHT-10:
            playerRect.move_ip(0,PLAYERMOVERATE)

        # 僵尸前进
        for z in zombies:
            z['rect'].move_ip(-1*NORMALZOMBIESPEED, 0)

        for c in newKindZombies:
            c['rect'].move_ip(-1*NEWKINDZOMBIESPEED,0)

        # 让子弹飞
        for b in bullets:
            b['rect'].move_ip(1 * BULLETSPEED, 0)

        # 僵尸达到终点
        for z in zombies[:]:
            if z['rect'].left < 0:
                zombies.remove(z)
                zombiesGottenPast += 1
        for c in newKindZombies[:]:
            if c['rect'].left <0:
                newKindZombies.remove(c)
                zombiesGottenPast += 1
		
		for b in bullets[:]:
			if b['rect'].right>WINDOWWIDTH:
				bullets.remove(b)
				
        # 检查子弹是否击中僵尸
        for z in zombies:
            if bulletHasHitZombie(bullets, zombies):
                score += 1
                zombies.remove(z)
    
        for c in newKindZombies:
            if bulletHasHitCrawler(bullets, newKindZombies):
                score += 1
                newKindZombies.remove(c)      

        # 画背景
        windowSurface.blit(rescaledBackground, (0, 0))

        # 玩家单位
        windowSurface.blit(playerImage, playerRect)

        # 僵尸单位
        for z in zombies:
            windowSurface.blit(z['surface'], z['rect'])

        for c in newKindZombies:
            windowSurface.blit(c['surface'], c['rect'])

        # 子弹
        for b in bullets:
            windowSurface.blit(b['surface'], b['rect'])

        # 分数显示
        drawText('zombies gotten past: %s' % (zombiesGottenPast), font, windowSurface, 10, 20)
        drawText('score: %s' % (score), font, windowSurface, 10, 50)

        # 更新显示
        pygame.display.update()
            
        # 检查是否有僵尸碰到玩家
        if playerHasHitZombie(playerRect, zombies):
            break
        if playerHasHitZombie(playerRect, newKindZombies):
           break
        
        # 检查未被拦截的僵尸总数是否超过设定值
        if zombiesGottenPast >= MAXGOTTENPASS:
            break

        mainClock.tick(FPS)

    # 游戏失败界面
    pygame.mixer.music.stop()
    gameOverSound.play()
    time.sleep(1)
    if zombiesGottenPast >= MAXGOTTENPASS:
        windowSurface.blit(rescaledBackground, (0, 0))
        windowSurface.blit(playerImage, (WINDOWWIDTH / 2, WINDOWHEIGHT - 70))
        drawText('score: %s' % (score), font, windowSurface, 10, 30)
        drawText('GAME OVER', font, windowSurface, (WINDOWWIDTH / 3), (WINDOWHEIGHT / 3))
        drawText('YOUR COUNTRY HAS BEEN DESTROIED', font, windowSurface, (WINDOWWIDTH / 4)- 80, (WINDOWHEIGHT / 3) + 100)
        drawText('Press enter to play again or escape to exit', font, windowSurface, (WINDOWWIDTH / 4) - 80, (WINDOWHEIGHT / 3) + 150)
        pygame.display.update()
        waitForPlayerToPressKey()
    if playerHasHitZombie(playerRect, zombies):
        windowSurface.blit(rescaledBackground, (0, 0))
        windowSurface.blit(playerImage, (WINDOWWIDTH / 2, WINDOWHEIGHT - 70))
        drawText('score: %s' % (score), font, windowSurface, 10, 30)
        drawText('GAME OVER', font, windowSurface, (WINDOWWIDTH / 3), (WINDOWHEIGHT / 3))
        drawText('YOU HAVE BEEN KISSED BY THE ZOMMBIE', font, windowSurface, (WINDOWWIDTH / 4) - 80, (WINDOWHEIGHT / 3) +100)
        drawText('Press enter to play again or escape to exit', font, windowSurface, (WINDOWWIDTH / 4) - 80, (WINDOWHEIGHT / 3) + 150)
        pygame.display.update()
        waitForPlayerToPressKey()
    gameOverSound.stop()
